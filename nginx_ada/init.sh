#!/bin/bash
DIRNAME="$(pwd)"


if [ ! -d /Users/rpr/Code/ansible/ansible/nginx_ada/group_vars ]; 
then
  mkdir ${DIRNAME}/group_vars
fi
if [ -e ~/.vault_pass.txt ]
then
  echo "Vault password file exists. Skipped"
else
  stty -echo
  echo -n -e "\nVaultpassword: "
  read vaultpass
  echo "${vaultpass}" > ~/.vault_pass.txt
  stty echo
fi

if [ -e ${DIRNAME}/group_vars/ada ]
then
  echo -e "\nGroup-Vars file ada exists. Skipped"
else
  echo -n -e "\nRemoteuser: "
  read remoteuser
  stty -echo
  echo -n -e "\nSSH/SUDO Password: "
  read pass
  stty echo
  echo "ansible_sudo_pass: '$pass'
ansible_ssh_pass: '$pass'
ansible_ssh_user: $remoteuser
remote_user: $remoteuser" > ${DIRNAME}/group_vars/ada
  echo 
  cd ${DIRNAME}/group_vars
  ansible-vault --vault-password-file=~/.vault_pass.txt encrypt ada
  cd ${DIRNAME} 
fi

