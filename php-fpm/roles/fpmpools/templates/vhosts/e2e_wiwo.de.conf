server {
    listen 22082;
    server_name
    e2e-kunde.wiwo.de
    e2e-archiv.wiwo.de
    e2e-emagazin.wiwo.de
    e2e-quiz.wiwo.de
    e2e-angebot.wiwo.de
    e2e-advent.wiwo.de
    e2e-assign.wiwo.de;

    access_log logs/e2e_wiwo.de-access.log main;
    error_log logs/e2e_wiwo.de-error.log;

    include serverfault.conf;
    include includes/proxy_remote_addr.inc;

    include maintenance_e2e_on.conf;

    client_max_body_size 25M;

    root html/wiwo.de;
    index index.html index.htm;

    if ($http_x_forwarded_proto != https) {
        return 301 https://$host$request_uri;
    }

    # E2E-Site needs IP restrictions
    include includes/E2E-restrictions.inc;

    # API Protection ARVATO-40 START
    include includes/apiprotection.e2e.inc;
    # API Protection ARVATO-40 ENDE

    location / {
        proxy_pass http://e2e_cgi_varnish;
    }
}

server {
    # Hier kein Force https
    listen 22082;
    server_name
    e2e-boerse.wiwo.de
    e2e-club.wiwo.de;

    access_log logs/e2e_wiwo.de-access.log main;
    error_log logs/e2e_wiwo.de-error.log;

    include serverfault.conf;
    include includes/proxy_remote_addr.inc;

    include maintenance_e2e_on.conf;

    client_max_body_size 25M;

    root html/wiwo.de;
    index index.html index.htm;

    # E2E-Site needs IP restrictions
#    include includes/E2E-restrictions.inc;

    location / {
	add_header Access-Control-Allow-Methods "GET, POST, PUT, DELETE, OPTIONS";
	add_header Access-Control-Allow-Headers "Authorization, Content-Type";
	add_header Access-Control-Allow-Credentials true;
	proxy_pass http://e2e_cgi_varnish;
    }
}


server {
    listen 22082;
    server_name ~^(?<prefix>bugfix|e2e|dev|dev2|dev3|dev4)(?<preview>-preview)?-wiwoapp.wiwo.de;

    access_log logs/e2e-wiwoapp.wiwo.de-access.log main;
    error_log logs/e2e-wiwoapp.wiwo.de-error.log;

    include servererror_upstream.conf;

    root html/e2e-wiwoapp.wiwo.de;
    index index.html index.htm;

    include includes/proxy_xforwardedfor.inc;

    if ($http_x_forwarded_proto != https) {
        return 301 https://$host$request_uri;
    }

    location ~ /apple-app-site-association {
        allow all;
        proxy_pass http://e2e_cm_varnish/$uri;
    }
    location ~ ^/public/(.*) {
        allow all;
        proxy_pass http://e2e_cm_varnish/public/$1$is_args$args;
    }
    location ~ ^/images/(.*) {
        allow all;
        proxy_pass http://e2e_cm_varnish/images/$1$is_args$args;
    }
    location ~ ^/css/(.*) {
        allow all;
        proxy_pass http://e2e_cm_varnish/css/$1$is_args$args;
    }
    location ~ ^/js/(.*) {
        allow all;
        proxy_pass http://e2e_cm_varnish/js/$1$is_args$args;
    }
    location ~ ^/downloads/(.*) {
        allow all;
        proxy_pass http://e2e_cm_varnish/downloads/$1$is_args$args;
    }
    location ~ /cmsid/(.*) {
        return 301 $scheme://$prefix$preview-www.wiwo.de/$1;
    }

    location /recommendation {
        proxy_pass http://e2e_cgi_varnish/recommendation;
    }

    # E2E-Site needs IP restrictions
    #        include includes/E2E-restrictions.inc;
    location / {
        auth_basic $auth_type_e2e;
        auth_basic_user_file auth/e2e.htpasswd;
        proxy_pass http://e2e_cm_varnish;
    }
}

server {
    listen 22082;
    server_name
	~^(?<prefix>bugfix|e2e|dev|dev2|dev3|dev4)(?<preview>-preview)?-amp.wiwo.de
	;

    access_log logs/e2e-amp-wiwo.de-access.log main;
    error_log logs/e2e-amp-wiwo.de-error.log;

    include servererror_upstream.conf;
    #include serverfault.conf;

    root html/wiwo.de;
    index index.html index.htm;

    # E2E-Site needs IP restrictions
    include includes/E2E-restrictions.inc;
    include includes/proxy_xforwardedfor.inc;

    include includes/cm_amp_e2e.inc;

    location / {
        expires 10m; # only cache 301 for 3 minutes
        return 301 $scheme://${prefix}${preview}-www.wiwo.de$request_uri;
    }

}


server {
    listen 22082;
    server_name
	~^(?<prefix>bugfix|e2e|dev|dev2|dev3|dev4)(?<preview>-preview)?-www.wiwo.de
	~^(?<prefix>bugfix|e2e|dev|dev2|dev3|dev4)(?<preview>-preview)?-beta.wiwo.de
	cdn-e2e-www.wiwo.de
	;

    access_log logs/e2e-wiwo.de-access.log main;
    error_log logs/e2e-wiwo.de-error.log;

    include servererror_upstream.conf;

    root html/wiwo.de;
    index index.html index.htm;

    # E2E-Site needs IP restrictions
    include includes/E2E-restrictions.inc;

    include includes/proxy_xforwardedfor.inc;

    include includes/redirects.launch.e2e.www.wiwo.de.inc;

        # Neues URL-Format fuer Bilder
        location ~ ^/images/(?<title>.+)/(?<id>[0-9]+)\.(?<ext>[a-zA-Z]+) {
                if ($args ~ format=(.+)){
                return 301 http://$host/images/$title/$id-$1.$ext;
                }
                return 301 http://$host/images/$title/$id-formatOriginal.$ext;
        }


    location /my {
        proxy_pass http://e2e_cgi_varnish/my;
    }

    location /ajaxentry/nocache {
        proxy_pass http://e2e_cgi_varnish/ajaxentry/nocache;
    }

    location /preparesite/empty.js {
        proxy_pass http://e2e_cgi_varnish/preparesite/empty.js;
    }

    location /meine-wirtschaftswoche {
        proxy_pass http://e2e_cgi_varnish/meine-wirtschaftswoche;
    }

    location /recommendation {
        proxy_pass http://e2e_cgi_varnish/recommendation;
    }

    location /uag-api {                                                                                                                                     
        proxy_pass http://e2e_cgi_varnish/uag-api;                                                                                                          
    } 

    location / {
	if ($http_x_forwarded_proto != https) {
		return 301 https://$host$request_uri;
	}
        proxy_pass http://e2e_cm_varnish;
    }
}

server {
    listen 22082;
    server_name

	    ~^(?<prefix>bugfix|e2e|dev|dev2|dev3|dev4)(?<preview>-preview)?-app.wiwo.de
	    cdn-e2e-app.wiwo.de
	;

    access_log logs/e2e-app.wiwo.de-access.log main;
    error_log logs/e2e-app.wiwo.de-error.log;

    include servererror_upstream.conf;

    # E2E-Site needs IP restrictions
    include includes/E2E-restrictions.inc;

    return 301 "https://${prefix}${preview}-www.wiwo.de$request_uri";
}
